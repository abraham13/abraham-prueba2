{
    "id": "6b9fa0c8-49fa-4dbd-8861-82198c00516a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_supermario",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Super Mario Bros. NES",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b0bf4234-b1b2-46c6-84db-a8b458375767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4ae1311b-2be0-4fb2-8dc4-8513ca8aaa4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 26,
                "y": 106
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d2648679-b4e1-4a27-9b83-8a23d337b08b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 71,
                "y": 93
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c77b8ead-e7be-403d-b832-b9b7548cce17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 80
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "095c92e7-e0dd-481f-8bd3-a2d723edae79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 41
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "86eda2ba-2b73-4782-873e-79c613aaa9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 11,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 110,
                "y": 41
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "833a05ae-57d6-4c56-a266-abe1e35fc863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 95,
                "y": 67
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ef5d1277-6bf4-451d-b807-a87cf7e0c175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 3,
                "x": 44,
                "y": 106
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7a85091a-971b-4536-bb14-89da4b9ec40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 10,
                "y": 106
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2c26f117-70e2-4f27-8fa8-7f1a0a13a1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 18,
                "y": 106
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7ece7bbc-6cad-4ad0-b452-06b1050dc401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 73,
                "y": 67
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5cc8e449-cbc8-49d2-9c16-ead599920014",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 80
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b5983cba-a80f-475d-91e8-5c974e8edff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 4,
                "x": 38,
                "y": 106
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "19ea85bf-14d7-4da2-a65f-fae8dc328916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 93
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3823373d-8444-4bf1-aac9-d94d7f4a6ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 3,
                "x": 59,
                "y": 106
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ef1c0984-b08b-4071-b811-fdcc5116f1bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fcd4f0a9-2ac1-472f-a569-cb297bda961c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bd4c9655-00dd-456b-94be-270fc79a111c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 93
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fc075104-0c01-4af9-aa34-b61b93cc0f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "597d2d92-a57b-401e-901d-321ad9b07abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 67
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "30d7eeeb-1505-4c0b-b228-4aa758ae70b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c9b43f85-65a3-4832-bc2c-6c8b8a11a2e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d9b844e5-0eb2-4c84-9a98-1481025a5b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 54
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "16792a59-3da5-44b2-a7ab-7d8b0d7cb98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 54
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bb208ef6-3acf-4551-84f6-c5b7e979afdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 54
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "358a0d84-5f58-41aa-b9c1-b58649460213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 54
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "980d11ef-e593-4081-9606-9ce227bca083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 3,
                "x": 64,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0e956c53-e984-4c73-97b1-c8205c2d6b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 4,
                "x": 32,
                "y": 106
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9c5f40f6-0fd7-4068-81ea-336d8c18f509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 80,
                "y": 93
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a54e2244-45a0-4058-a18e-b53c68b662a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 80
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ca2e4cd3-e0de-4b6e-aeff-784967806919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 62,
                "y": 93
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "70e78300-0626-47b1-9341-246dd1de741f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 67
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c44a7061-0fb6-433d-a50e-85a20509a19d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 11,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 67
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "436d8ab9-f68b-4d8d-8db6-8e8e3cf8fc01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 67
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "90c8c906-10cb-4503-b6c1-acc6be56ab08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 41
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4b9eed96-30ad-4ff7-aed3-12357f0f0fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "61908e3a-2267-412f-b935-6b392649c1dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 41
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3139f50e-cd57-477a-b11c-42c54a214d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 41
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "64c4cfd8-4dbf-4030-ad30-618c071c25fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 15
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2d69bb72-48e3-42e1-8a9c-8c7fc4aebe49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6c008b83-080a-43d1-a76f-8d505246b89d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 15
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "290068c6-42e2-4fc8-967f-1fc8a4abac85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 93
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "54b2dfcd-04a2-4849-8c62-4ba450627854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 15
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1428e98c-e434-4491-8485-c05a617a6f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 15
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4eabae9d-5883-49e1-a8d8-7672f99eb1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d288d6ff-3490-4ee1-a2bb-0533e09a2c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 15
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9afd5043-31f4-43ec-afed-5adc2ff9dcf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "89541856-fdf5-43a5-aaa2-9f1b707f4dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 41
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "37ab55c1-7354-43cb-b4aa-81aa8a61ded0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 15
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3750bbf2-c789-4464-8185-4b0726fec193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4505ef63-945d-4bf1-afa3-f39268269f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 106,
                "y": 67
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b4fa23f1-737e-4b7e-8c9d-67099cc9ad67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "26e6f972-47ea-48a0-901c-80f1f04861de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 93
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e97a1b02-104f-4767-9aff-e08dbd703590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cac7b1b4-4dde-4b50-b611-a497080b6da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "dc502760-a046-47aa-9dd4-db1fd4c2bfde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fe618227-1280-436a-ac41-e78ec7d3f7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a0066bc0-99e3-4aa3-ba57-40d31b45d791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 93
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e06102f2-90e0-4609-a2a3-077288ff9604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 24,
                "y": 80
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "80432be8-8123-4ff3-9b3b-33d32e4f2b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 114,
                "y": 93
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "24271733-11f0-4822-b818-f9eaa668f8f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "152c78b6-3870-4b21-a638-698b45633463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0d01f1f0-b71a-4286-a47c-e21c79243423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 89,
                "y": 93
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f8d60088-1260-4e5b-8ebf-1ee8243f0837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 67
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4c26016f-1378-41e1-84ca-56b8f1ad4a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 2,
                "shift": 10,
                "w": 3,
                "x": 49,
                "y": 106
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6b0b460c-6dac-4da2-a30c-ecf195134ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b5c32733-0d7c-4319-b25f-3cec8873f26f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 15
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "94f53fa4-9e8b-42e2-8da1-32f3789809c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 15
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "28366676-427c-46e6-8fe5-1ac0faf5eeab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 15
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8a793c99-fd26-4569-b513-d32cc1016828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 41
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "43b3d582-4d76-42fd-8901-7c399b8296eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 41
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "dd9eb60f-0022-4c27-b3d4-3782c681322c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 80
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aa90f117-6fe8-416e-8ec1-82b39a43e47e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 41
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d0cb11aa-adbd-4b95-8c5e-9b27df89193c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 93
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "21c12ae1-d2b0-41ec-b7ee-64993580de6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5569447e-cf91-46dd-8b3e-b8ac2206e515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6f9c08a2-2395-48fd-94df-1542cb177e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 62,
                "y": 67
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "51af085b-56f6-412e-b2e3-d8cf6c002e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "be5a70a4-4907-4b59-bb78-22e35137d8e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 28
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d3e490fd-6bb2-4d78-a1fd-e0fc197eefd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d61c9596-e533-4b5c-b6a2-01e4da304e01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9d9f7b11-ab79-4ff7-9c78-280ec69fd07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "132961c2-be2e-4f32-94de-e46064dc05c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "38ffe042-258e-4a43-b05e-00d57f4a7c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ac3b59a9-1744-468f-bca0-6746d9bb9416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 90,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6d13fa96-56b0-4534-bdb2-742c9835c0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "af1c88a5-09bf-45d1-8749-7e20f1d1f10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "63856e93-d04b-4bad-aa62-67c7272b3013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3117d0af-0dde-4670-a616-4964c3a05734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 15
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c9fb3c8d-6083-4b42-9e69-eafb7d5a571f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 100,
                "y": 80
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4b0d347d-ed23-4a71-b1d9-524b0a674f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 80
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "94b8637f-a343-4127-b672-e8497c42a9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 98,
                "y": 93
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "65c9339b-8e88-489d-8986-8e407d102662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 11,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 54,
                "y": 106
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "44feff6a-d72c-4372-9ccf-92af7f85db4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 106,
                "y": 93
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2045f1fc-fc66-4bc6-9531-27f9fefe8759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 67
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}