{
    "id": "34397ceb-e74f-4b2f-9d7e-bd8715d34acb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06b8659c-3d08-466e-970c-385da7cc07a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34397ceb-e74f-4b2f-9d7e-bd8715d34acb",
            "compositeImage": {
                "id": "1214311c-3de5-4e13-a3e7-5e23635909d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b8659c-3d08-466e-970c-385da7cc07a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ec591ec-950c-4efe-94b6-a71b20abc902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b8659c-3d08-466e-970c-385da7cc07a9",
                    "LayerId": "d38a72c5-b3a0-4246-a66d-8e0683a4a4ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "d38a72c5-b3a0-4246-a66d-8e0683a4a4ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34397ceb-e74f-4b2f-9d7e-bd8715d34acb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}