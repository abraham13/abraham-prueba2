{
    "id": "c7c3b208-cbcb-4602-b863-a02842cbeae4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enewalk2rojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5b97f4d-4868-4d5f-b020-2d8fcc254a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7c3b208-cbcb-4602-b863-a02842cbeae4",
            "compositeImage": {
                "id": "8108c6d2-977a-44a1-90ad-c825f20fcc5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b97f4d-4868-4d5f-b020-2d8fcc254a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44cad5c9-b225-4070-a427-8cf05d263d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b97f4d-4868-4d5f-b020-2d8fcc254a0f",
                    "LayerId": "3fb20597-8e1e-4361-8a2d-1f63a2b12478"
                }
            ]
        },
        {
            "id": "dd5a4790-f3ba-4ab9-b236-95e0e95570de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7c3b208-cbcb-4602-b863-a02842cbeae4",
            "compositeImage": {
                "id": "d5bdb386-dfcb-44f7-9700-464a90b67ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5a4790-f3ba-4ab9-b236-95e0e95570de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c213ffc-71c7-4106-9557-99a7025fba01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5a4790-f3ba-4ab9-b236-95e0e95570de",
                    "LayerId": "3fb20597-8e1e-4361-8a2d-1f63a2b12478"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3fb20597-8e1e-4361-8a2d-1f63a2b12478",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7c3b208-cbcb-4602-b863-a02842cbeae4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}