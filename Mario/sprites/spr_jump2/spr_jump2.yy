{
    "id": "ac3fbd8e-acb6-41c2-93ff-4a47d7a05abf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dc7011c-8536-4573-9257-2bf95c271b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac3fbd8e-acb6-41c2-93ff-4a47d7a05abf",
            "compositeImage": {
                "id": "a1aa11eb-1bbc-4d1a-b991-d0011a250eac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc7011c-8536-4573-9257-2bf95c271b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c3d563-c9e3-4a16-978c-b44e692fc8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc7011c-8536-4573-9257-2bf95c271b1e",
                    "LayerId": "e56a7de1-85bd-45c1-8cc5-6ab8e3b14787"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "e56a7de1-85bd-45c1-8cc5-6ab8e3b14787",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac3fbd8e-acb6-41c2-93ff-4a47d7a05abf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}