{
    "id": "bc3239da-3e9c-4ef5-8da5-83cb8f9769da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walkizquierda",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf5a3d3f-c9a8-420d-afbd-e9a817cc84ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3239da-3e9c-4ef5-8da5-83cb8f9769da",
            "compositeImage": {
                "id": "14f66e97-7b13-45ab-9a6b-8f612b66b61d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf5a3d3f-c9a8-420d-afbd-e9a817cc84ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9d55658-c8ab-4075-b2de-06ab5b8a24e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf5a3d3f-c9a8-420d-afbd-e9a817cc84ca",
                    "LayerId": "a8b20e45-0d96-405d-8592-f3447ba21702"
                }
            ]
        },
        {
            "id": "e7546561-e5be-4e3a-957c-091bba9d82fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc3239da-3e9c-4ef5-8da5-83cb8f9769da",
            "compositeImage": {
                "id": "885535f3-acd1-434c-8cf3-71696071314d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7546561-e5be-4e3a-957c-091bba9d82fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eae89ed-fdef-4324-8958-10631f8671a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7546561-e5be-4e3a-957c-091bba9d82fd",
                    "LayerId": "a8b20e45-0d96-405d-8592-f3447ba21702"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "a8b20e45-0d96-405d-8592-f3447ba21702",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc3239da-3e9c-4ef5-8da5-83cb8f9769da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}