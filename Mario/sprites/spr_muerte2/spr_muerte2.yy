{
    "id": "da7231b1-0f54-4467-8e7c-2be75ce73cb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_muerte2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0c3d788-8874-4866-aa68-c550ef16d25a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7231b1-0f54-4467-8e7c-2be75ce73cb1",
            "compositeImage": {
                "id": "b3ebaa2e-a242-41c4-95bd-7b593ffa25bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0c3d788-8874-4866-aa68-c550ef16d25a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1187e907-c2f5-4bff-aa4b-cefc020e25eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0c3d788-8874-4866-aa68-c550ef16d25a",
                    "LayerId": "3561375a-6c87-42a6-9e2a-7eac67792e7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "3561375a-6c87-42a6-9e2a-7eac67792e7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da7231b1-0f54-4467-8e7c-2be75ce73cb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}