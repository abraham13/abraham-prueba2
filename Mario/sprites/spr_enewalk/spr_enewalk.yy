{
    "id": "6b39ffb8-6055-4d6b-a5e2-4e5aa9fc8943",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enewalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10b5f694-9c2a-475e-862d-8b1c671bfca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b39ffb8-6055-4d6b-a5e2-4e5aa9fc8943",
            "compositeImage": {
                "id": "7f3416b1-d617-4795-bbe8-5e6d5cdfd6d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b5f694-9c2a-475e-862d-8b1c671bfca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc8811e-ab35-4609-bf34-d0a73b2c8ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b5f694-9c2a-475e-862d-8b1c671bfca5",
                    "LayerId": "06f95291-774f-4079-adac-50411e9becc2"
                }
            ]
        },
        {
            "id": "840f3aee-bd9b-4cb4-949e-2c2c9b5d7344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b39ffb8-6055-4d6b-a5e2-4e5aa9fc8943",
            "compositeImage": {
                "id": "5ad48ac5-cc58-4e2b-b6a7-ffe2f0fcc01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "840f3aee-bd9b-4cb4-949e-2c2c9b5d7344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0981e8-c5ff-48c2-ac4e-e3a1d01aa830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "840f3aee-bd9b-4cb4-949e-2c2c9b5d7344",
                    "LayerId": "06f95291-774f-4079-adac-50411e9becc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "06f95291-774f-4079-adac-50411e9becc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b39ffb8-6055-4d6b-a5e2-4e5aa9fc8943",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}