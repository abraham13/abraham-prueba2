{
    "id": "480baacd-dbe1-4f4c-97ef-0fad3c4375d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c826aad-d4f9-4ccb-b2d4-2b4c2c73595f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "480baacd-dbe1-4f4c-97ef-0fad3c4375d4",
            "compositeImage": {
                "id": "d0c87689-9a6d-4dbc-9aec-f4794f0b8d03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c826aad-d4f9-4ccb-b2d4-2b4c2c73595f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec51051-abce-4eba-a43c-ab4bbf6c7317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c826aad-d4f9-4ccb-b2d4-2b4c2c73595f",
                    "LayerId": "8b7d5f8c-318b-459b-adab-afbee2642565"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "8b7d5f8c-318b-459b-adab-afbee2642565",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "480baacd-dbe1-4f4c-97ef-0fad3c4375d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}