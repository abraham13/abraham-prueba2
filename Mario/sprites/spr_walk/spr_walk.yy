{
    "id": "bc538588-9027-49e0-82c9-53f449b3b724",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a64b8f0-7278-4e7a-ae90-485990860784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc538588-9027-49e0-82c9-53f449b3b724",
            "compositeImage": {
                "id": "fe1790f1-991b-4fcd-a7f9-975f3cd8ec92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a64b8f0-7278-4e7a-ae90-485990860784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25bc0503-eaa4-49a6-bfe6-6bc2cbca7f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a64b8f0-7278-4e7a-ae90-485990860784",
                    "LayerId": "7d839df8-bb0c-4c11-9172-2628b160656e"
                }
            ]
        },
        {
            "id": "9837169c-a29f-4085-bac9-b6d3d1d67aed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc538588-9027-49e0-82c9-53f449b3b724",
            "compositeImage": {
                "id": "bd25aa86-3448-4957-9fbd-5acd66c4edcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9837169c-a29f-4085-bac9-b6d3d1d67aed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d570c1-5fb4-4d10-a3aa-9db5e089ed7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9837169c-a29f-4085-bac9-b6d3d1d67aed",
                    "LayerId": "7d839df8-bb0c-4c11-9172-2628b160656e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "7d839df8-bb0c-4c11-9172-2628b160656e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc538588-9027-49e0-82c9-53f449b3b724",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}