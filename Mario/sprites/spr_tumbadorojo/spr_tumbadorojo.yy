{
    "id": "5f11269f-c606-4a81-b92d-3c2755431e92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tumbadorojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e08d6af6-8707-45c9-8b5f-ec2784a5d3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f11269f-c606-4a81-b92d-3c2755431e92",
            "compositeImage": {
                "id": "53a68d3b-7742-41d0-8453-786c3c51005c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08d6af6-8707-45c9-8b5f-ec2784a5d3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8179e45-1070-4a60-a13f-a91cd0f5a17b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08d6af6-8707-45c9-8b5f-ec2784a5d3db",
                    "LayerId": "0daca710-c447-40f2-bcf3-9cc860e514ef"
                }
            ]
        },
        {
            "id": "be11d98a-affa-4f32-bbf7-e3629c68fd91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f11269f-c606-4a81-b92d-3c2755431e92",
            "compositeImage": {
                "id": "e2c697fc-9c59-4436-9e33-664d40311270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be11d98a-affa-4f32-bbf7-e3629c68fd91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38760752-64c3-446d-b771-874e30d3473a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be11d98a-affa-4f32-bbf7-e3629c68fd91",
                    "LayerId": "0daca710-c447-40f2-bcf3-9cc860e514ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0daca710-c447-40f2-bcf3-9cc860e514ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f11269f-c606-4a81-b92d-3c2755431e92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}