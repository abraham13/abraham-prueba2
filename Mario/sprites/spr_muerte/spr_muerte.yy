{
    "id": "c693b1f9-bb98-4515-be4b-ef7d7ddaa383",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_muerte",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52c12ef4-2030-40de-9afc-a9954f3aaddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c693b1f9-bb98-4515-be4b-ef7d7ddaa383",
            "compositeImage": {
                "id": "4b3be325-7977-4aee-84eb-93066847acfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c12ef4-2030-40de-9afc-a9954f3aaddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9cd1c1f-498a-4eb1-820c-e232ad278bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c12ef4-2030-40de-9afc-a9954f3aaddc",
                    "LayerId": "c0415f05-1792-4398-9eab-e5dce43848c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "c0415f05-1792-4398-9eab-e5dce43848c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c693b1f9-bb98-4515-be4b-ef7d7ddaa383",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}