{
    "id": "82c64b28-3512-4157-a46e-8ab76775e8e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titulo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a7062f6-30f4-4094-98b7-190fd539455f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82c64b28-3512-4157-a46e-8ab76775e8e7",
            "compositeImage": {
                "id": "ab87721e-a8b3-42c4-899b-1bf4140f24bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a7062f6-30f4-4094-98b7-190fd539455f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43057b6b-0884-4cd8-95e7-ba9095207ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a7062f6-30f4-4094-98b7-190fd539455f",
                    "LayerId": "46fcc1f0-9f4e-4f94-889d-94ab61666197"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "46fcc1f0-9f4e-4f94-889d-94ab61666197",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82c64b28-3512-4157-a46e-8ab76775e8e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 0,
    "yorig": 0
}