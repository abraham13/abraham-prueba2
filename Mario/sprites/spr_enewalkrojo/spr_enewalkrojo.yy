{
    "id": "19518ec4-291c-40dc-9de9-15286614184f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enewalkrojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fb26895-4eb3-4752-b271-044424266c49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19518ec4-291c-40dc-9de9-15286614184f",
            "compositeImage": {
                "id": "775a8b58-d29f-4536-b770-b1082aeaa87e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb26895-4eb3-4752-b271-044424266c49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99534b85-62dd-400d-846d-399508973ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb26895-4eb3-4752-b271-044424266c49",
                    "LayerId": "2f1a9d88-7b73-4957-bf2a-219e0c508529"
                }
            ]
        },
        {
            "id": "1d571a57-baab-4f01-8f00-8d7573860dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19518ec4-291c-40dc-9de9-15286614184f",
            "compositeImage": {
                "id": "1a4b2849-ca75-4a6d-b036-760e7f2cc1bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d571a57-baab-4f01-8f00-8d7573860dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68157ba8-2d59-4db3-9179-ed686c2ffc92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d571a57-baab-4f01-8f00-8d7573860dc3",
                    "LayerId": "2f1a9d88-7b73-4957-bf2a-219e0c508529"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2f1a9d88-7b73-4957-bf2a-219e0c508529",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19518ec4-291c-40dc-9de9-15286614184f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}