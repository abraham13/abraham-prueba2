{
    "id": "d005c303-ae3d-416a-a5fa-45fef84aeacd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enewalk2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89d626d4-d133-41c8-a72e-39a706d56dab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d005c303-ae3d-416a-a5fa-45fef84aeacd",
            "compositeImage": {
                "id": "031d80ff-d53f-4f8d-99f2-a921998f5046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d626d4-d133-41c8-a72e-39a706d56dab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "319b11d3-98dd-4f58-bd2b-9f8ee75752c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d626d4-d133-41c8-a72e-39a706d56dab",
                    "LayerId": "61294771-a6d0-46d4-820e-5ff93056c12f"
                }
            ]
        },
        {
            "id": "113fb52d-b11c-478d-85e8-08a62f97f8e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d005c303-ae3d-416a-a5fa-45fef84aeacd",
            "compositeImage": {
                "id": "b0008cf6-5be1-4d6c-ac6b-8b653d4e5143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "113fb52d-b11c-478d-85e8-08a62f97f8e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "571a6c73-c6b8-4ae6-a77f-ae91b9dc6bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "113fb52d-b11c-478d-85e8-08a62f97f8e5",
                    "LayerId": "61294771-a6d0-46d4-820e-5ff93056c12f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "61294771-a6d0-46d4-820e-5ff93056c12f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d005c303-ae3d-416a-a5fa-45fef84aeacd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}