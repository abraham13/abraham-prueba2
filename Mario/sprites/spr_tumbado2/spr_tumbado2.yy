{
    "id": "b70c69eb-4e72-4461-875c-5715cd02321e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tumbado2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "546107a7-b1ed-48a3-9228-d5d2d4368389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b70c69eb-4e72-4461-875c-5715cd02321e",
            "compositeImage": {
                "id": "07fed571-ae87-4765-89b0-460e1cc0f891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "546107a7-b1ed-48a3-9228-d5d2d4368389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcdb4e7f-92f3-4975-a114-2edc979f6d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "546107a7-b1ed-48a3-9228-d5d2d4368389",
                    "LayerId": "d4d978c1-f51d-4709-8112-8669effb361b"
                }
            ]
        },
        {
            "id": "2a510098-c22a-479f-9701-db5b62b2b65d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b70c69eb-4e72-4461-875c-5715cd02321e",
            "compositeImage": {
                "id": "104f7703-d052-420c-b38d-bb1aaff3b593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a510098-c22a-479f-9701-db5b62b2b65d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a245fd3-9ac8-4d27-91c0-6638cc75c918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a510098-c22a-479f-9701-db5b62b2b65d",
                    "LayerId": "d4d978c1-f51d-4709-8112-8669effb361b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d4d978c1-f51d-4709-8112-8669effb361b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b70c69eb-4e72-4461-875c-5715cd02321e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}