{
    "id": "f08e5551-18d8-4db7-9712-b1c3b1343408",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk2izquierdo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74291313-47f4-452d-8957-af8ea3d127c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f08e5551-18d8-4db7-9712-b1c3b1343408",
            "compositeImage": {
                "id": "8c2ee2b0-48bd-48a6-845d-7d61d79794be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74291313-47f4-452d-8957-af8ea3d127c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f99fc4-7ea2-49c0-b36e-e92b415a1aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74291313-47f4-452d-8957-af8ea3d127c7",
                    "LayerId": "e6b7ea68-0333-4a1e-9997-1f23d76649c3"
                }
            ]
        },
        {
            "id": "39cf20c6-3a19-40e5-abed-8f87fbcf9012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f08e5551-18d8-4db7-9712-b1c3b1343408",
            "compositeImage": {
                "id": "b3678f3a-cae5-42e3-a9d9-1a68381b092d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39cf20c6-3a19-40e5-abed-8f87fbcf9012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b9e25c-ff4e-4547-89ab-8dc7022d532a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39cf20c6-3a19-40e5-abed-8f87fbcf9012",
                    "LayerId": "e6b7ea68-0333-4a1e-9997-1f23d76649c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "e6b7ea68-0333-4a1e-9997-1f23d76649c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f08e5551-18d8-4db7-9712-b1c3b1343408",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 11
}