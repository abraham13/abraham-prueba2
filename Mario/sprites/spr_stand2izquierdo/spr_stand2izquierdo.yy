{
    "id": "294fe1dc-ff06-4c3a-b8c2-3353bd773df1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stand2izquierdo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13a2deaf-1556-432a-bcc8-7d177c4d60fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "294fe1dc-ff06-4c3a-b8c2-3353bd773df1",
            "compositeImage": {
                "id": "cb001652-9da5-4e66-80d0-85b11175a01d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a2deaf-1556-432a-bcc8-7d177c4d60fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6eb99b-701e-4403-b438-da918123f0dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a2deaf-1556-432a-bcc8-7d177c4d60fa",
                    "LayerId": "32f93e4f-d1cd-4ca4-9e98-bfcd60fb08c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "32f93e4f-d1cd-4ca4-9e98-bfcd60fb08c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "294fe1dc-ff06-4c3a-b8c2-3353bd773df1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}