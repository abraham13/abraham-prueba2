{
    "id": "5ec5d341-13ca-45c0-85d4-aef802fdc140",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43daade4-5178-4b73-a4f9-85fe19abb343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec5d341-13ca-45c0-85d4-aef802fdc140",
            "compositeImage": {
                "id": "c5caeb48-7730-467b-9bba-59a472f2cc77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43daade4-5178-4b73-a4f9-85fe19abb343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5040b3b2-34d9-457f-82c4-4ac2efb1501b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43daade4-5178-4b73-a4f9-85fe19abb343",
                    "LayerId": "7f952603-303b-4da2-9dd7-90f1a50df6e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "7f952603-303b-4da2-9dd7-90f1a50df6e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ec5d341-13ca-45c0-85d4-aef802fdc140",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 268,
    "yorig": 346
}