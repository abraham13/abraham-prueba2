{
    "id": "ac7a658f-e2f2-4a50-8436-a090e550da2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stand2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffa57030-b543-4116-a981-067472dbbd56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac7a658f-e2f2-4a50-8436-a090e550da2e",
            "compositeImage": {
                "id": "40ca6aee-4c4f-4d00-b8af-5d83d7e3587b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa57030-b543-4116-a981-067472dbbd56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec1621c-04e3-4ba4-8a7d-be4db038cac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa57030-b543-4116-a981-067472dbbd56",
                    "LayerId": "f864bec2-9b62-49ef-a85f-f53277c6bfe3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "f864bec2-9b62-49ef-a85f-f53277c6bfe3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac7a658f-e2f2-4a50-8436-a090e550da2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}