{
    "id": "495f19cd-93c3-4d44-ae8d-368b6dfa3d14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemigoMuertorojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "333084b5-59e9-4b4a-ac41-ce176d9cbbcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "495f19cd-93c3-4d44-ae8d-368b6dfa3d14",
            "compositeImage": {
                "id": "2e222cef-886d-4b4a-9efe-314ebc8412ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "333084b5-59e9-4b4a-ac41-ce176d9cbbcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64879dcc-fd3a-43c0-b0e2-2f8739909c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "333084b5-59e9-4b4a-ac41-ce176d9cbbcb",
                    "LayerId": "e3129275-0a71-4f0c-878b-79b5d625a88a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e3129275-0a71-4f0c-878b-79b5d625a88a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "495f19cd-93c3-4d44-ae8d-368b6dfa3d14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}