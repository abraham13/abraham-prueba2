{
    "id": "0a248cde-4d80-44f1-9a74-6adc0f197f68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump2izquierdo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c464bd25-5304-4313-9ccf-80c8702f591f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a248cde-4d80-44f1-9a74-6adc0f197f68",
            "compositeImage": {
                "id": "ab58f9c2-ca66-4a3b-89d3-97ae1da38c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c464bd25-5304-4313-9ccf-80c8702f591f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "730c66ea-7a64-4f98-936e-eae0d80e7f4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c464bd25-5304-4313-9ccf-80c8702f591f",
                    "LayerId": "71867549-7260-4a7a-ab5b-fd644386acae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "71867549-7260-4a7a-ab5b-fd644386acae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a248cde-4d80-44f1-9a74-6adc0f197f68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}