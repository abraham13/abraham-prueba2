{
    "id": "a532c112-6b7d-4002-8783-3faff3b2e380",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_money",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7c8bc69-d75f-4bc6-b8d2-bd090f8fcfe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
            "compositeImage": {
                "id": "cb4a28c0-9e92-4654-b48d-b25235728dcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7c8bc69-d75f-4bc6-b8d2-bd090f8fcfe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "298f54f0-e4c2-420b-bcea-727488547a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7c8bc69-d75f-4bc6-b8d2-bd090f8fcfe4",
                    "LayerId": "f1648fc7-545d-46b6-ba2d-f9a0fbde2a22"
                }
            ]
        },
        {
            "id": "cf8658c4-fee2-479a-9b9f-2057873b8fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
            "compositeImage": {
                "id": "1db08f36-dc0d-4e2e-b29e-3f0133ba0863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf8658c4-fee2-479a-9b9f-2057873b8fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "989cd1b1-1751-4f02-bd99-176595c07fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf8658c4-fee2-479a-9b9f-2057873b8fe6",
                    "LayerId": "f1648fc7-545d-46b6-ba2d-f9a0fbde2a22"
                }
            ]
        },
        {
            "id": "95b296e6-7470-4b3c-8c83-47678a5d1f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
            "compositeImage": {
                "id": "a08e6c0b-1432-4a8e-86ff-e33c3d29e54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b296e6-7470-4b3c-8c83-47678a5d1f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "422a506e-3b1d-4834-9d50-f69e37fe6438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b296e6-7470-4b3c-8c83-47678a5d1f46",
                    "LayerId": "f1648fc7-545d-46b6-ba2d-f9a0fbde2a22"
                }
            ]
        },
        {
            "id": "297ac1da-41b8-4354-ab7e-ec8932b6e683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
            "compositeImage": {
                "id": "29e8035a-a9a0-44a2-a7bb-da8a5b387483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "297ac1da-41b8-4354-ab7e-ec8932b6e683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b40b86c3-c29c-409d-98fb-b50eb310b334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "297ac1da-41b8-4354-ab7e-ec8932b6e683",
                    "LayerId": "f1648fc7-545d-46b6-ba2d-f9a0fbde2a22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "f1648fc7-545d-46b6-ba2d-f9a0fbde2a22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 1,
    "yorig": 5
}