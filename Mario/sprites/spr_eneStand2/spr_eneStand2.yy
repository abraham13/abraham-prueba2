{
    "id": "85f04063-2b35-4ef0-a639-39c7729f4531",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_eneStand2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50a0b044-3e43-4e35-a50e-bd9a733fe244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85f04063-2b35-4ef0-a639-39c7729f4531",
            "compositeImage": {
                "id": "1de73865-f703-48de-976a-0978d152daa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50a0b044-3e43-4e35-a50e-bd9a733fe244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa4c9614-2b36-4fe0-9fa6-455211bd309d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a0b044-3e43-4e35-a50e-bd9a733fe244",
                    "LayerId": "7c3039c1-4fe6-4d5f-b4d3-0f198d9660d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7c3039c1-4fe6-4d5f-b4d3-0f198d9660d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85f04063-2b35-4ef0-a639-39c7729f4531",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}