{
    "id": "1e26f673-b107-4fca-8b7f-e644c87d561b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemigoMuerto2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05d83d07-2637-4e5c-8beb-13ea27f669e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e26f673-b107-4fca-8b7f-e644c87d561b",
            "compositeImage": {
                "id": "8a9a95d1-7f99-40e5-a142-c4f14ddbb0bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d83d07-2637-4e5c-8beb-13ea27f669e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c10687f-f54e-4f83-83f1-41921359400c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d83d07-2637-4e5c-8beb-13ea27f669e2",
                    "LayerId": "4eb4f790-49ad-41a1-9e84-2cd2d41b8490"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4eb4f790-49ad-41a1-9e84-2cd2d41b8490",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e26f673-b107-4fca-8b7f-e644c87d561b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}