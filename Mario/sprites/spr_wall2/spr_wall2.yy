{
    "id": "30cf4992-51a5-4b2a-ada8-2a991d83cfe6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37675064-cb81-4da0-a46b-d342c82f4f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30cf4992-51a5-4b2a-ada8-2a991d83cfe6",
            "compositeImage": {
                "id": "30b01617-bfe6-45d5-9d6a-6d71b8cfa96a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37675064-cb81-4da0-a46b-d342c82f4f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35ff17f-d5fb-4e44-a645-f0439a9045ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37675064-cb81-4da0-a46b-d342c82f4f51",
                    "LayerId": "5db70c30-763b-4ad9-8a7e-bf3935534b46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "5db70c30-763b-4ad9-8a7e-bf3935534b46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30cf4992-51a5-4b2a-ada8-2a991d83cfe6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 14,
    "yorig": 7
}