{
    "id": "0adec6f4-89df-4186-aabf-c75e364a4153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_standizquierdo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0352586d-7fcf-414f-bfce-afe421bab5d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0adec6f4-89df-4186-aabf-c75e364a4153",
            "compositeImage": {
                "id": "f9910576-9360-4ad6-bc97-29831866cc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0352586d-7fcf-414f-bfce-afe421bab5d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ef866c-323a-459d-a215-7edcaabdbd76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0352586d-7fcf-414f-bfce-afe421bab5d2",
                    "LayerId": "bb3b0952-9501-4d1a-968f-e15dd860aeb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "bb3b0952-9501-4d1a-968f-e15dd860aeb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0adec6f4-89df-4186-aabf-c75e364a4153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}