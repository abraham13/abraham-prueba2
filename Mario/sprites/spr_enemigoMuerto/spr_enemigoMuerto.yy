{
    "id": "27cf30cb-8802-4b15-a1db-c635a877aebc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemigoMuerto",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e306f124-9aec-443e-a307-16e9491dce25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27cf30cb-8802-4b15-a1db-c635a877aebc",
            "compositeImage": {
                "id": "6bb3e259-7561-4383-ab52-91a5c5f13712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e306f124-9aec-443e-a307-16e9491dce25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "932066a5-3f40-467e-9dcc-2023a8ceb536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e306f124-9aec-443e-a307-16e9491dce25",
                    "LayerId": "4c187917-3041-4c95-a697-40a1e6309821"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4c187917-3041-4c95-a697-40a1e6309821",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27cf30cb-8802-4b15-a1db-c635a877aebc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}