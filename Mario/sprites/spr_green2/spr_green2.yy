{
    "id": "d1a8b3e0-a7e4-4002-ad43-e239bfdb2aba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdf24cea-b17c-4b5c-acb9-66c78bfdbe2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1a8b3e0-a7e4-4002-ad43-e239bfdb2aba",
            "compositeImage": {
                "id": "95fbcd2c-a8ff-4961-b4f4-63dbd262e959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf24cea-b17c-4b5c-acb9-66c78bfdbe2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f1b1b9c-0b69-4272-b78c-e02c763d5a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf24cea-b17c-4b5c-acb9-66c78bfdbe2a",
                    "LayerId": "1410a207-6b26-4dc2-93af-7325d7be1f44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "1410a207-6b26-4dc2-93af-7325d7be1f44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1a8b3e0-a7e4-4002-ad43-e239bfdb2aba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 268,
    "yorig": 346
}