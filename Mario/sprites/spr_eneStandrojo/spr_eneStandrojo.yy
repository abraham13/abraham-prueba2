{
    "id": "97ed2f59-9864-4c9b-8bd8-f080c48bc4e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_eneStandrojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f50dac20-f35e-47bb-a9a5-531d16d6152e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed2f59-9864-4c9b-8bd8-f080c48bc4e4",
            "compositeImage": {
                "id": "6e1ed246-716a-4780-b423-98656a344ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f50dac20-f35e-47bb-a9a5-531d16d6152e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed11b2f6-616e-4ea2-8761-31b3c400588f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f50dac20-f35e-47bb-a9a5-531d16d6152e",
                    "LayerId": "564e9061-ea6f-446b-ab45-6b9a937fa087"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "564e9061-ea6f-446b-ab45-6b9a937fa087",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97ed2f59-9864-4c9b-8bd8-f080c48bc4e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}