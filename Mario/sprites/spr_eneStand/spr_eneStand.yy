{
    "id": "9531c2ff-21ae-43b4-8cbd-485b130f5933",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_eneStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19a8ff0d-a7d7-4e49-94a8-358dbe407fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9531c2ff-21ae-43b4-8cbd-485b130f5933",
            "compositeImage": {
                "id": "92ce1920-9769-4497-aa18-b36ce7153dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a8ff0d-a7d7-4e49-94a8-358dbe407fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9da3f69-c6c1-47df-8df8-cf0feffb279b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a8ff0d-a7d7-4e49-94a8-358dbe407fc1",
                    "LayerId": "d11f9351-4928-4224-9032-304ac395a093"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d11f9351-4928-4224-9032-304ac395a093",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9531c2ff-21ae-43b4-8cbd-485b130f5933",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}