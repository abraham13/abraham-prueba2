{
    "id": "eab7c141-73a6-4f4e-95e3-28b01f5f145a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EnemigoMuerto2rojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46eb71e4-437a-4bc8-abb0-6084c6fb3cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab7c141-73a6-4f4e-95e3-28b01f5f145a",
            "compositeImage": {
                "id": "258f8535-0cea-4b9b-9c76-b08379c6d7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46eb71e4-437a-4bc8-abb0-6084c6fb3cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1adddec2-d394-407f-8783-3d7c7d62bb08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46eb71e4-437a-4bc8-abb0-6084c6fb3cd0",
                    "LayerId": "77186cf2-7b9f-4baf-a75a-0f7dbfdc9eab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "77186cf2-7b9f-4baf-a75a-0f7dbfdc9eab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eab7c141-73a6-4f4e-95e3-28b01f5f145a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}