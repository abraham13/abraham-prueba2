{
    "id": "63e9645c-d0d7-467e-96d2-d786e4435455",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09e7e57d-4774-4458-afd2-e9a3c6afb7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63e9645c-d0d7-467e-96d2-d786e4435455",
            "compositeImage": {
                "id": "c484dc36-f94a-4030-8ce7-492df3c1b92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09e7e57d-4774-4458-afd2-e9a3c6afb7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af03f40d-152b-4e07-8dfa-395be9191e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09e7e57d-4774-4458-afd2-e9a3c6afb7b3",
                    "LayerId": "c548e761-9e0a-4372-a9e5-c765be874176"
                }
            ]
        },
        {
            "id": "3b59c223-7c25-4180-8a3b-a2e8deed837e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63e9645c-d0d7-467e-96d2-d786e4435455",
            "compositeImage": {
                "id": "097366e4-1737-4806-a5cc-51d9d1ffb428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b59c223-7c25-4180-8a3b-a2e8deed837e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0080f60f-4f65-426a-a6d7-4c6dc2b5daa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b59c223-7c25-4180-8a3b-a2e8deed837e",
                    "LayerId": "c548e761-9e0a-4372-a9e5-c765be874176"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "c548e761-9e0a-4372-a9e5-c765be874176",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63e9645c-d0d7-467e-96d2-d786e4435455",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 11
}