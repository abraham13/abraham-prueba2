{
    "id": "c6886703-ad6e-4347-81e6-f8db650188f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tumbado2rojo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd2b9606-a376-443e-9f23-9aa82321b834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6886703-ad6e-4347-81e6-f8db650188f9",
            "compositeImage": {
                "id": "338bdacd-dc18-42f8-98a9-671cc30aad6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd2b9606-a376-443e-9f23-9aa82321b834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "add54152-8d30-464e-8ea4-2c4f5d4dea9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd2b9606-a376-443e-9f23-9aa82321b834",
                    "LayerId": "8c735580-feeb-421e-be2c-d7b0323f7b78"
                }
            ]
        },
        {
            "id": "45d2373b-b803-43ff-9276-ba1fd81e19b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6886703-ad6e-4347-81e6-f8db650188f9",
            "compositeImage": {
                "id": "ebf87fb3-ad37-4e92-abba-7de3dbcbe960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d2373b-b803-43ff-9276-ba1fd81e19b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1068030d-66d2-437f-9d16-7946ac246af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d2373b-b803-43ff-9276-ba1fd81e19b4",
                    "LayerId": "8c735580-feeb-421e-be2c-d7b0323f7b78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8c735580-feeb-421e-be2c-d7b0323f7b78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6886703-ad6e-4347-81e6-f8db650188f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}