{
    "id": "986f137c-9cd8-4db4-b371-7a7c58d8f297",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jumpizquierdo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5aa8281-c8e9-463d-9d8a-9ea28db2f153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "986f137c-9cd8-4db4-b371-7a7c58d8f297",
            "compositeImage": {
                "id": "e69ce6b6-5f12-41db-98e5-39c9e6ee740b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5aa8281-c8e9-463d-9d8a-9ea28db2f153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e94c7b-5fca-422b-96e1-8b6ae227c5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5aa8281-c8e9-463d-9d8a-9ea28db2f153",
                    "LayerId": "5ee181eb-3c73-4fff-9107-fdad1687ce08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "5ee181eb-3c73-4fff-9107-fdad1687ce08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "986f137c-9cd8-4db4-b371-7a7c58d8f297",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}