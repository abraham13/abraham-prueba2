{
    "id": "fb1387c6-4b40-4476-9e40-6d3c2879eb78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_eneStand21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2658fb1f-062c-47ab-bcb1-8dad24cc14eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb1387c6-4b40-4476-9e40-6d3c2879eb78",
            "compositeImage": {
                "id": "2e1aeccd-d3f9-4cd4-9afc-140541747393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2658fb1f-062c-47ab-bcb1-8dad24cc14eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d2c1b6b-3dd3-4531-95c9-85ecdedc7729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2658fb1f-062c-47ab-bcb1-8dad24cc14eb",
                    "LayerId": "3735fabb-2bbe-4384-a5ae-8f278666d85c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3735fabb-2bbe-4384-a5ae-8f278666d85c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb1387c6-4b40-4476-9e40-6d3c2879eb78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 8
}