{
    "id": "700c8e0e-1c40-4da2-b667-8460ed2ff246",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tumbado",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a511c36-d02d-4f1b-8a92-967a41931127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "700c8e0e-1c40-4da2-b667-8460ed2ff246",
            "compositeImage": {
                "id": "f8271931-cc42-40f5-9492-dbe6ee8c4790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a511c36-d02d-4f1b-8a92-967a41931127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a804e723-6596-456b-b09b-f92e0a746e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a511c36-d02d-4f1b-8a92-967a41931127",
                    "LayerId": "83c179db-21a4-421a-b4db-89069560b945"
                }
            ]
        },
        {
            "id": "ce49124a-764c-42c0-bc78-d00975afaa4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "700c8e0e-1c40-4da2-b667-8460ed2ff246",
            "compositeImage": {
                "id": "c7054e7b-2456-402c-8fd2-bd793188217f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce49124a-764c-42c0-bc78-d00975afaa4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ceaaa7-5b47-4059-b1c1-e728c1bafd0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce49124a-764c-42c0-bc78-d00975afaa4c",
                    "LayerId": "83c179db-21a4-421a-b4db-89069560b945"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "83c179db-21a4-421a-b4db-89069560b945",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "700c8e0e-1c40-4da2-b667-8460ed2ff246",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 8
}