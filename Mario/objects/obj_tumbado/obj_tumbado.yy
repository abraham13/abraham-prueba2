{
    "id": "6ef43c97-b14f-4ccb-bada-ba7ccda33675",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tumbado",
    "eventList": [
        {
            "id": "2aa2357e-daab-4f4d-a5f5-b97a9b3f0433",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6ef43c97-b14f-4ccb-bada-ba7ccda33675"
        },
        {
            "id": "71eb4cc6-4bba-4f75-bd26-f4f8b0692a7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6ef43c97-b14f-4ccb-bada-ba7ccda33675"
        },
        {
            "id": "6a909acd-71b5-453c-86bc-52f3aa21e08d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6ef43c97-b14f-4ccb-bada-ba7ccda33675"
        }
    ],
    "maskSpriteId": "b70c69eb-4e72-4461-875c-5715cd02321e",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b70c69eb-4e72-4461-875c-5715cd02321e",
    "visible": true
}