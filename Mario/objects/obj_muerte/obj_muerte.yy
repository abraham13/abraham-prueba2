{
    "id": "b785ace6-9106-474d-90d2-ab356eb920f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_muerte",
    "eventList": [
        {
            "id": "da7c11b6-412a-4be0-bcbf-8afc7923dc3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b785ace6-9106-474d-90d2-ab356eb920f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c693b1f9-bb98-4515-be4b-ef7d7ddaa383",
    "visible": true
}