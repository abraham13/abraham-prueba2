{
    "id": "7f51d43f-799e-4358-a3a1-b5214698cfee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_malo2rojo",
    "eventList": [
        {
            "id": "678ce5f1-2e9b-4b7e-942c-42c3c05e3f6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        },
        {
            "id": "2e7c386e-2d81-403e-a47d-70282548b26c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        },
        {
            "id": "45638f52-bd73-4c5e-a371-1884b39e152b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        },
        {
            "id": "e3e4bc37-a949-430d-89c8-aacdb2371b1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        },
        {
            "id": "cdb26b72-d973-451e-81e6-8682916499dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        },
        {
            "id": "328cd241-60a2-40e8-9f96-8fdb80336b66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        },
        {
            "id": "d5fda200-9c64-4fe4-ae08-5f1b08370082",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "808b59c8-bb99-4932-ad3d-86836883b0d5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f51d43f-799e-4358-a3a1-b5214698cfee"
        }
    ],
    "maskSpriteId": "fb1387c6-4b40-4476-9e40-6d3c2879eb78",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb1387c6-4b40-4476-9e40-6d3c2879eb78",
    "visible": true
}