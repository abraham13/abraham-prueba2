{
    "id": "808b59c8-bb99-4932-ad3d-86836883b0d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall2",
    "eventList": [
        {
            "id": "f6e96b23-c662-4ed2-8b05-c8b7fac5abc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "808b59c8-bb99-4932-ad3d-86836883b0d5"
        },
        {
            "id": "b0b3fe0a-301b-4ecf-a8f1-06ff1d7f710c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "808b59c8-bb99-4932-ad3d-86836883b0d5"
        }
    ],
    "maskSpriteId": "30cf4992-51a5-4b2a-ada8-2a991d83cfe6",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30cf4992-51a5-4b2a-ada8-2a991d83cfe6",
    "visible": true
}