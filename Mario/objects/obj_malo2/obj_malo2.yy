{
    "id": "1fc18aa0-9b06-41f8-a4a7-da9106876606",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_malo2",
    "eventList": [
        {
            "id": "ba950682-0b04-4632-aac6-920d00cf681c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        },
        {
            "id": "1064e06e-e4f3-4b31-82e4-7bf0f0a3ec00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        },
        {
            "id": "9be58762-4970-49ef-929c-1569e8135694",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        },
        {
            "id": "b8a0474a-ac9d-4d0b-ac19-8795bdbe4181",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a4209645-1848-45f0-9447-183bd5e22fae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        },
        {
            "id": "ad54fb71-c636-4e1d-a634-2e1add20ec4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        },
        {
            "id": "2100fb01-de2f-4cc0-ac80-5744612f4b25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        },
        {
            "id": "8d274ba6-a010-4fcb-ba2f-0991bd08b0b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1fc18aa0-9b06-41f8-a4a7-da9106876606"
        }
    ],
    "maskSpriteId": "85f04063-2b35-4ef0-a639-39c7729f4531",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85f04063-2b35-4ef0-a639-39c7729f4531",
    "visible": true
}