{
    "id": "f8f0e510-810e-4bf7-83f5-b4fe46fd1a8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green2rojo",
    "eventList": [
        {
            "id": "8f0a1d31-1063-4a00-a769-f08e69c5a971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f8f0e510-810e-4bf7-83f5-b4fe46fd1a8a"
        },
        {
            "id": "8189693b-9820-42a8-a317-6711200ef340",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f8f0e510-810e-4bf7-83f5-b4fe46fd1a8a"
        },
        {
            "id": "e0ce67ff-a1ab-4535-ba3d-5e9581307422",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "911c748e-bbdd-4d71-a502-460b47c74c3d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f8f0e510-810e-4bf7-83f5-b4fe46fd1a8a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1a8b3e0-a7e4-4002-ad43-e239bfdb2aba",
    "visible": true
}