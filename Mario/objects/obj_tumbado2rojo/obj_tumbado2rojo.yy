{
    "id": "8d86dd82-fb5b-4ecf-9fa6-3fa2582e7428",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tumbado2rojo",
    "eventList": [
        {
            "id": "1c1a2f47-2390-4bb1-9734-284bac3be488",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d86dd82-fb5b-4ecf-9fa6-3fa2582e7428"
        },
        {
            "id": "51ce4366-29b9-4dae-8941-e4eb354ad285",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8d86dd82-fb5b-4ecf-9fa6-3fa2582e7428"
        },
        {
            "id": "01e1de41-2b0c-422d-80bd-472abfa996a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8d86dd82-fb5b-4ecf-9fa6-3fa2582e7428"
        }
    ],
    "maskSpriteId": "5f11269f-c606-4a81-b92d-3c2755431e92",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f11269f-c606-4a81-b92d-3c2755431e92",
    "visible": true
}