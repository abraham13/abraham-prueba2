{
    "id": "2d3481cf-be7c-4544-9951-6ccb5284acfc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green3",
    "eventList": [
        {
            "id": "b97d03fe-953a-4951-8b4b-82ce78a95c74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1fc18aa0-9b06-41f8-a4a7-da9106876606",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2d3481cf-be7c-4544-9951-6ccb5284acfc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1a8b3e0-a7e4-4002-ad43-e239bfdb2aba",
    "visible": true
}