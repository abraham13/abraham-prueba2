{
    "id": "7a0fee6a-dd5f-4ef8-800b-b7fdc6843f1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green11",
    "eventList": [
        {
            "id": "1dd5ebc3-a1cc-44f5-83b3-85b1dc2f6c64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7a0fee6a-dd5f-4ef8-800b-b7fdc6843f1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ec5d341-13ca-45c0-85d4-aef802fdc140",
    "visible": true
}