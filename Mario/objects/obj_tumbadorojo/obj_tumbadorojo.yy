{
    "id": "07831f9c-69a8-4cec-b94c-300c942dc61e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tumbadorojo",
    "eventList": [
        {
            "id": "57081284-68a8-472d-9fa6-c8a480bfbe1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "07831f9c-69a8-4cec-b94c-300c942dc61e"
        },
        {
            "id": "fbce588b-20e9-4c4d-ac2e-5ddd2c768d55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "07831f9c-69a8-4cec-b94c-300c942dc61e"
        },
        {
            "id": "92d397c8-c723-4f4a-9cd2-6edbcba73b44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "07831f9c-69a8-4cec-b94c-300c942dc61e"
        }
    ],
    "maskSpriteId": "c6886703-ad6e-4347-81e6-f8db650188f9",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c6886703-ad6e-4347-81e6-f8db650188f9",
    "visible": true
}