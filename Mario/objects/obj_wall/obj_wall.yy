{
    "id": "a4209645-1848-45f0-9447-183bd5e22fae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall",
    "eventList": [
        {
            "id": "87574c1c-6da9-4295-8a4a-f3e56f3de00f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a4209645-1848-45f0-9447-183bd5e22fae"
        },
        {
            "id": "e5a5075c-1ab2-4eca-aa02-885dd7b5b384",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a4209645-1848-45f0-9447-183bd5e22fae"
        }
    ],
    "maskSpriteId": "295555d7-6822-48ff-a335-53a0814d19db",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "295555d7-6822-48ff-a335-53a0814d19db",
    "visible": true
}