{
    "id": "911c748e-bbdd-4d71-a502-460b47c74c3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_malorojo",
    "eventList": [
        {
            "id": "5d75c9b1-2f7f-42d2-806a-c74a921d15a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        },
        {
            "id": "6c72758c-7ce1-47fa-8289-dc3850584ceb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        },
        {
            "id": "8d6c4e22-9a28-4569-b00e-efaa95f4bd21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        },
        {
            "id": "fd38a377-5e28-44a1-bc1f-d10f4e63cee9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        },
        {
            "id": "e7381116-1768-4f66-997a-d42572bc3682",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        },
        {
            "id": "593d94ff-2771-4d95-ac7a-f203e6b568e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        },
        {
            "id": "2482ac26-61e3-405d-9e19-9564dfad10c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "808b59c8-bb99-4932-ad3d-86836883b0d5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "911c748e-bbdd-4d71-a502-460b47c74c3d"
        }
    ],
    "maskSpriteId": "97ed2f59-9864-4c9b-8bd8-f080c48bc4e4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97ed2f59-9864-4c9b-8bd8-f080c48bc4e4",
    "visible": true
}