{
    "id": "569be0c7-2ae7-488b-91c0-0dae84ca0d98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_muerteEnemigo",
    "eventList": [
        {
            "id": "6cd1b3a0-a042-495a-bd88-1a2ce1418e89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "569be0c7-2ae7-488b-91c0-0dae84ca0d98"
        }
    ],
    "maskSpriteId": "27cf30cb-8802-4b15-a1db-c635a877aebc",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27cf30cb-8802-4b15-a1db-c635a877aebc",
    "visible": true
}