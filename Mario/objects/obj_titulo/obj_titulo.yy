{
    "id": "9c6b5567-28d2-4075-898f-94c3f95e47cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_titulo",
    "eventList": [
        {
            "id": "b7ca2e8a-3f97-4709-8c3e-b766d2fb4aca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c6b5567-28d2-4075-898f-94c3f95e47cf"
        },
        {
            "id": "9d1e1743-2ffb-44d6-9d06-577b5d1dea3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9c6b5567-28d2-4075-898f-94c3f95e47cf"
        },
        {
            "id": "46fd3b64-14c0-4ec8-be97-e87bed84f23a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9c6b5567-28d2-4075-898f-94c3f95e47cf"
        },
        {
            "id": "db3f268f-c54b-42ec-8905-7247e0f57613",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "9c6b5567-28d2-4075-898f-94c3f95e47cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}