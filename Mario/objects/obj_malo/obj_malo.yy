{
    "id": "42ae6fcd-d72c-4a18-8911-334c61f02cfe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_malo",
    "eventList": [
        {
            "id": "0c04cd78-cf45-4906-a29d-484e99ff21cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        },
        {
            "id": "7ed7635c-cce6-4ddc-85e5-555ae8f14fb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        },
        {
            "id": "84ab2783-c82f-41e0-a2a2-9adaabc49d80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        },
        {
            "id": "f98b92f9-4a84-47b2-9205-908d9e0c685c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a4209645-1848-45f0-9447-183bd5e22fae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        },
        {
            "id": "9a661159-742e-4639-8424-5c18a719e4b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        },
        {
            "id": "b24cf206-b8c9-4750-8f39-9e321f81540e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        },
        {
            "id": "1a1e36fb-fb2e-4d00-b468-a818a17d3def",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "42ae6fcd-d72c-4a18-8911-334c61f02cfe"
        }
    ],
    "maskSpriteId": "9531c2ff-21ae-43b4-8cbd-485b130f5933",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9531c2ff-21ae-43b4-8cbd-485b130f5933",
    "visible": true
}