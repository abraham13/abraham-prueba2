{
    "id": "42f54e8f-2de9-48db-ab88-956b7f5146c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_moneda",
    "eventList": [
        {
            "id": "0b9b039b-2633-409a-9ff2-48b412d9f221",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "42f54e8f-2de9-48db-ab88-956b7f5146c4"
        },
        {
            "id": "ad78cc3b-b474-4a2a-8afb-5d9ade674e7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42f54e8f-2de9-48db-ab88-956b7f5146c4"
        },
        {
            "id": "80a6ca35-11a1-4242-931a-c6b24af1286e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42f54e8f-2de9-48db-ab88-956b7f5146c4"
        },
        {
            "id": "20a5ba82-e980-4d26-a0c5-9299b1cd9ac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42f54e8f-2de9-48db-ab88-956b7f5146c4"
        },
        {
            "id": "4a63cbaa-8a1d-4b16-a67b-e619645cb2e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "42f54e8f-2de9-48db-ab88-956b7f5146c4"
        }
    ],
    "maskSpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
    "visible": true
}