{
    "id": "68bc8c91-7cc1-4015-a220-699114937d51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenrojo",
    "eventList": [
        {
            "id": "53b7b70d-1768-4ffc-9279-27cd3610e876",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68bc8c91-7cc1-4015-a220-699114937d51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ec5d341-13ca-45c0-85d4-aef802fdc140",
    "visible": true
}