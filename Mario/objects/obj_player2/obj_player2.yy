{
    "id": "cd09a6a1-8945-4d67-be2f-17826522b6f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2",
    "eventList": [
        {
            "id": "e3f62d74-9074-4531-8bb6-fd726607ee17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "13e5635b-a6a0-4d7c-92f3-4d345bfecb1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "aa6f92da-3239-4f72-b226-4bb61260aace",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "a7b473c3-1f94-4bac-9875-6f7a77d0a748",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "42ae6fcd-d72c-4a18-8911-334c61f02cfe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "fad83823-7cae-4011-a866-d8598d997a4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1fc18aa0-9b06-41f8-a4a7-da9106876606",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "5c0dd0d0-97d8-4e8f-868a-c10dfba84df1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "42f54e8f-2de9-48db-ab88-956b7f5146c4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "7c9eaa33-1a02-40d2-9160-763d479069c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "ae98344f-18a5-4681-9454-d3fe27771503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7f51d43f-799e-4358-a3a1-b5214698cfee",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        },
        {
            "id": "85243360-82ef-450e-8559-cb84e16edf63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "911c748e-bbdd-4d71-a502-460b47c74c3d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cd09a6a1-8945-4d67-be2f-17826522b6f7"
        }
    ],
    "maskSpriteId": "480baacd-dbe1-4f4c-97ef-0fad3c4375d4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ac7a658f-e2f2-4a50-8436-a090e550da2e",
    "visible": true
}