{
    "id": "6f3f77f1-eb12-4869-9c6a-39a8c7e4fd0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green",
    "eventList": [
        {
            "id": "fcb5bd4e-8d22-4505-a95f-578f045adf1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f3f77f1-eb12-4869-9c6a-39a8c7e4fd0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5ec5d341-13ca-45c0-85d4-aef802fdc140",
    "visible": true
}