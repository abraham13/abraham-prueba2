{
    "id": "adf8e8be-066c-45b0-8432-bc4006047491",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green2",
    "eventList": [
        {
            "id": "1984b4cc-3f90-40cc-ae87-c52e2514ce11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "42ae6fcd-d72c-4a18-8911-334c61f02cfe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "adf8e8be-066c-45b0-8432-bc4006047491"
        },
        {
            "id": "f98bef63-c634-4dcc-aae7-324d9bb75621",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adf8e8be-066c-45b0-8432-bc4006047491"
        },
        {
            "id": "cdf6e2d0-02a3-40f1-9c41-37ae288c9574",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "adf8e8be-066c-45b0-8432-bc4006047491"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1a8b3e0-a7e4-4002-ad43-e239bfdb2aba",
    "visible": true
}