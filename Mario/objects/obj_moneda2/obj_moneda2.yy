{
    "id": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_moneda2",
    "eventList": [
        {
            "id": "2090ce78-7eca-4993-b7c5-13633ebe849b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238"
        },
        {
            "id": "4a2053ac-2956-438c-ad08-153ae87582e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238"
        },
        {
            "id": "f6a75f5a-96fb-4b8b-889f-2e60683261be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238"
        },
        {
            "id": "32dd330d-ae32-4779-9258-d71031fe6fcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238"
        },
        {
            "id": "036a7e5f-52e2-42c1-a89c-42e6730569f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238"
        }
    ],
    "maskSpriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a532c112-6b7d-4002-8783-3faff3b2e380",
    "visible": true
}