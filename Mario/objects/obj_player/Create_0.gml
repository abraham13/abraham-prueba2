/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 11351DD5
/// @DnDArgument : "code" "grav = 0.2;$(13_10)hsp = 0;$(13_10)vsp = 0;$(13_10)$(13_10)jumpspeed = 7;$(13_10)movespeed = 4;$(13_10)$(13_10)//Constants$(13_10)grounded = false;$(13_10)jumping = false;$(13_10)$(13_10)//Horizontal jump constants$(13_10)hsp_jump_constant_small = 1;$(13_10)hsp_jump_constant_big = 4;$(13_10)hsp_jump_applied = 0;$(13_10)$(13_10)//Horizontal key pressed count$(13_10)hkp_count = 0;$(13_10)hkp_count_small = 2;$(13_10)hkp_count_big = 5;$(13_10)$(13_10)//Init variables$(13_10)key_left = 0;$(13_10)key_right = 0;$(13_10)key_jump = false;$(13_10)$(13_10)$(13_10)$(13_10)"
grav = 0.2;
hsp = 0;
vsp = 0;

jumpspeed = 7;
movespeed = 4;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;