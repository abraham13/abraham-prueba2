{
    "id": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "7d17704b-7eaa-4f31-860d-15c707c49e33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "29f5a8da-59cb-42cf-b5a7-e39f6a3ab2f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "eaa1079b-3203-4806-bcf9-17899032b3cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "d00c923a-9186-4a52-a0dc-bd0510102c59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "42ae6fcd-d72c-4a18-8911-334c61f02cfe",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "fd2a572b-4961-4640-842c-6e7c3c28b513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "1fc18aa0-9b06-41f8-a4a7-da9106876606",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "13c6c726-1453-4cbb-9b35-907c158802d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "42f54e8f-2de9-48db-ab88-956b7f5146c4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "6b2e8e55-1b02-4157-8cfa-34a67fe0d48f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e4e31c38-ed8c-4aa9-8a95-f1a58311c238",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "cd96f6ff-46c7-46ef-8168-4faa3ce2539c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7f51d43f-799e-4358-a3a1-b5214698cfee",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        },
        {
            "id": "823b0286-ab6a-44c6-91f0-f423f8e8c73a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "911c748e-bbdd-4d71-a502-460b47c74c3d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8d6b9f2-48eb-41c2-a6c4-36e24d4f03bb"
        }
    ],
    "maskSpriteId": "480baacd-dbe1-4f4c-97ef-0fad3c4375d4",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "480baacd-dbe1-4f4c-97ef-0fad3c4375d4",
    "visible": true
}